---
title: "About FLP"
description: "A few years ago, while visiting or, rather, rummaging about Notre-Dame, the author of this book found, in an obscure nook of one of the towers, the following word, engraved by hand upon the wall: —ANANKE."
featured_image: '/images/Victor_Hugo-Hunchback.jpg'
menu:
  main:
    weight: 1
---

### About Future Leaders Program

Developed in partnership with the Education Changes Lives and Times (ECLAT) Foundation, the Future Leaders Program and Future Leaders Camp aim to provide a comprehensive learning experience for students who are interested in becoming a global leader. The programs also emphasize the importance of networking and connecting students with peers and professionals from around the world to create a supportive and dynamic global community.

### Who

- Founded in 2018 by the ECLAT Foundation, the Future Leaders Program has 53 Alumni.
- Collaborated with Fu Jen Catholic University, Taiwan
- Collaborated with Wenzao University, Taiwan

### What

The Future Leaders Program (FLP) is a two-week virtual program delivered through MS Teams. Students gain a solid foundation in global business, build a global network, and develop problem-solving skills, including building actionable social impact plans that align with the Sustainable Development Goals (SDGs).

The Future Leaders Camp (FLC) is a two-week in-person program conducted in Texas. It provides students with a unique opportunity to visit multinational companies, interact with executives across various industries, and participate in a variety of cultural activities.
